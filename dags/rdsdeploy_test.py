from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators import DummyOperator, BashOperator
from airflow.models import Variable
from datetime import date

default_args = {
            'owner': 'airflow-master',
                'depends_on_past': False,
                    'start_date': datetime(2019,9,5),
                        'email': ['airflow@example.com'],
                            'email_on_failure': False,
                                'email_on_retry': False,
                                    'retries': 0,
                                        'retry_delay': timedelta(minutes=1),
                                            'schedule_interval': '@once',
                                            }

dag = DAG('Deploy_Test', default_args=default_args, schedule_interval='@once')



Start = DummyOperator(
            task_id='Start',
                retries=2,
                        dag=dag
                        )

End = DummyOperator(
            task_id='End',
                retries=1,
                        dag=dag
                        )
t1 = BashOperator(
            task_id='RDS_CONFIG_CHANGE',
                bash_command='echo $PWD',
                    retries=1,
                        dag=dag)

Start>>t1>>End