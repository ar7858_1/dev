from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators import DummyOperator, BashOperator
from airflow.models import Variable
from datetime import date

default_args = {
            'owner': 'airflow-master',
                'depends_on_past': False,
                    'start_date': datetime(2019,9,5),
                        'email': ['airflow@example.com'],
                            'email_on_failure': False,
                                'email_on_retry': False,
                                    'retries': 0,
                                        'retry_delay': timedelta(minutes=1),
                                            'schedule_interval': '@once',
                                            }

dag = DAG('parallel_test_DAG', default_args=default_args, schedule_interval='@once')



Start = DummyOperator(
            task_id='Start',
                retries=1,
                        dag=dag
                        )

End = DummyOperator(
            task_id='End',
                retries=1,
                        dag=dag
                        )
t1 = BashOperator(
            task_id='t1',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						
						
t2 = BashOperator(
            task_id='t2',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)



t3 = BashOperator(
            task_id='t3',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						
						
						
						
						
t4 = BashOperator(
            task_id='t4',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)




t5 = BashOperator(
            task_id='t5',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						
						
						
						
t6 = BashOperator(
            task_id='t6',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						
						
						
t7 = BashOperator(
            task_id='t7',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						
						
							
						
t8 = BashOperator(
            task_id='t8',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)


						
						
t9 = BashOperator(
            task_id='t9',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						

t10 = BashOperator(
            task_id='t9',
                bash_command='sleep 2',
                    retries=1,
                        dag=dag)
						


				
Start>>[t1,t2,t3,t4,t5,t6,t7,t8,t9,t10]>>End

